package General;

import javax.swing.JOptionPane;
import java.lang.NumberFormatException;

/**
 *
 * @author Rubén Martínez Cabello 71421914-Z
 */

public class ejercicio12
{
    protected static void enunciado()
    {
        System.out.println("Ejercicio 12");
        System.out.println("Realiza una aplicación que nos pida un número de ventas a introducir, después nos pedirá tantas ventas por teclado como número de ventas se hayan indicado. Al final mostrara la suma de todas las ventas. Piensa que es lo que se repite y lo que no.");
        System.out.println("================================================================================");
    }    

    public static void variante01(String[] args) 
    {
        Integer i, ventas, cantidad, total;
        String str;
        
        ventas = 0;
        cantidad = 0;
        total = 0;
        
        try
        {
            ventas = Integer.parseInt( JOptionPane.showInputDialog("¿Número de ventas?") );
        }
        catch(NumberFormatException nfe)
        {
            System.out.println( "Lo siento, no te entiendo.");
        }
        finally
        {
            System.out.println( "Asi que has hecho " + ventas + " ventas.");
        };
        
        for (i = 1; i<= ventas; i++)
        {
            do
            {
                str = JOptionPane.showInputDialog("En la venta número "+ i +" ¿Cuántos productos se han vendido?");
                try {

                    cantidad = Integer.parseInt( str );
                }
                catch (NumberFormatException nfe)
                {
                    System.out.println( "Lo siento, no te entiendo. ¿" + str + "?");
                    continue;
                };
                break;
            }
            while (true);
            total += cantidad;
        }
        
        System.out.println( "Has vendido " + String.valueOf(total) + " productos en total.");
        
    }    
}