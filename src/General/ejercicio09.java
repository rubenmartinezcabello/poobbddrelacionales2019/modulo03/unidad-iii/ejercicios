package General;

import javax.swing.JOptionPane;

/**
 *
 * @author Rubén Martínez Cabello 71421914-Z
 */

public class ejercicio09 
{
    protected static void enunciado()
    {
        System.out.println("Ejercicio 09");
        System.out.println("Muestra los números del 1 al 100 (ambos incluidos). Usa un bucle WHILE.");
        System.out.println("================================================================================");
    }    

    public static void variante01(String[] args) 
    {
        Integer acumulador = 1;
        while (acumulador<=100)
        {
            System.out.println(acumulador++);
        };
    }
}