package General;

import javax.swing.JOptionPane;

/**
 *
 * @author Rubén Martínez Cabello 71421914-Z
 */

public class ejercicio02 
{
    public static Integer número = 2;
    public static String  enunciado = "Declara 2 variables numéricas (con el valor que desees), e indica cual es mayor de los dos. Si son iguales indicarlo también. Prueba a cambiar los valores para comprobar que funciona.";

    public static void printEnunciado()
    {
        System.out.format ("Ejercicio %02d%n", ejercicio01.número);
        System.out.println(ejercicio01.enunciado);
        System.out.println("================================================================================");
    }
    
    public static void variante01(String[] args) 
    {
        String texto;
        float[] operando;
        
        operando = new float[2];
        operando[0] = Float.parseFloat(JOptionPane.showInputDialog("Introduce el primer número:"));
        operando[1] = Float.parseFloat(JOptionPane.showInputDialog("Introduce el segundo número:"));
        
        System.out.println( "Comparando " + String.valueOf(operando[0]) + " con " + String.valueOf(operando[1]) + " .");
        if (operando[0]>operando[1])
        {
            System.out.println( "El mayor es el primero, " + String.valueOf(operando[0]) );
        }
        else if (operando[0]<operando[1])
        {
            System.out.println( "El mayor es el segundo, " + String.valueOf(operando[1]) );
        }
        else if (operando[0] == operando[1])
        {
            System.out.println( "Son iguales (" + String.valueOf(operando[0]) + ")" );
        }
        else
        {
            System.out.println( "Algo raro sucede." );
        }
        operando = null;
    }
        
}
