package General;

/**
 *
 * @author Rubén Martínez Cabello 71421914-Z
 *
**/


public class ejercicio03
{
    public static Integer número = 3;
    public static String  enunciado = "Declara un String que contenga tu nombre, después muestra un mensaje de bienvenida por consola. Por ejemplo: si introduzco “Fernando”, me aparezca 'Bienvenido Fernando'.";

    public static void printEnunciado()
    {
        System.out.format ("Ejercicio %02d%n", ejercicio01.número);
        System.out.println(ejercicio01.enunciado);
        System.out.println("================================================================================");
    }  
    
    public static void variante01(String[] args) 
    {
        String nombre = "Rubén";
        System.out.println( "Bienvenido, " + nombre);
    }

    
}
