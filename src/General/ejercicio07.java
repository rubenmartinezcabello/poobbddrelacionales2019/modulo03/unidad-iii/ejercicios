package General;

import java.util.Locale;
import java.text.NumberFormat;
import java.text.ParseException;
import javax.swing.JOptionPane;

/**
 *
 * @author Rubén Martínez Cabello 71421914-Z
 * 
**/

public class ejercicio07 
{
    protected static void enunciado()
    {
        System.out.println("Ejercicio 07");
        System.out.println("Lee un número por teclado que pida el precio de un producto (puede tener decimales) y calcule el precio final con IVA. El IVA será una constante que será del 21%.");
        System.out.println("================================================================================");
    }    
    
    
    public static void variante01(String[] args, Locale currentLocale) 
    {
        NumberFormat nf = NumberFormat.getInstance(currentLocale);
        String s;
        Double precio;
        final Double IVA = 0.21;
        
        try {
            s = JOptionPane.showInputDialog("Introduce el precio (puede contener decimales):");
            precio = nf.parse(s).doubleValue();
        }
        catch(ParseException pe ) {
            precio = 0.0;
        }
        catch (Exception e)
        {
            precio = 0.0;
        }
        finally 
        {
        };
        
        JOptionPane.showMessageDialog(null, "El precio es " + nf.format(precio * (1.0+IVA)), "P.V.P.", JOptionPane.INFORMATION_MESSAGE);
    }
}
