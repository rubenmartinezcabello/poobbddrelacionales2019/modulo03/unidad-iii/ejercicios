package General;

import javax.swing.JOptionPane;

/**
 *
 * @author Rubén Martínez Cabello 71421914-Z
 */

public class ejercicio10 
{
    protected static void enunciado()
    {
        System.out.println("Ejercicio 10");
        System.out.println("Muestra los números del 1 al 100 (ambos incluidos). Usa un bucle FOR.");
        System.out.println("================================================================================");
    }    

    public static void variante01(String[] args) 
    {
        Integer acumulador = 1;
        for ( acumulador = 1; acumulador<=100; acumulador++)
        {
            System.out.println(acumulador);
        };

    }    
}