package General;

import javax.swing.JOptionPane;

/**
 *
 * @author Rubén Martínez Cabello 71421914-Z
 */

public class ejercicio01 
{
    public static Integer número = 1;
    public static String  enunciado = "Declara dos variables numéricas (con el valor que desees), muestra por consola la suma, resta, multiplicación, división y módulo (resto de la división).";
    
    public static void printEnunciado()
    {
        System.out.format ("Ejercicio %02d%n", ejercicio01.número);
        System.out.println(ejercicio01.enunciado);
        System.out.println("================================================================================");
    }    

    public static void variante01(String[] args) 
    {

        int operando0, operando1;

        operando0 = 3;
        operando1 = 2;

        System.out.println( "Operando: " + String.valueOf(operando0) );
        System.out.println( "Operando: " + String.valueOf(operando1) );
        System.out.println( "------------------" );
        System.out.println( "Suma    : " + String.valueOf(operando0 + operando1) );
        System.out.println( "Resta   : " + String.valueOf(operando0 - operando1) );
        System.out.println( "Producto: " + String.valueOf(operando0 * operando1) );
        System.out.println( "Cociente: " + String.valueOf(operando0 / operando1) );
        System.out.println( "Resto   : " + String.valueOf(operando0 % operando1) );
        System.out.println();
        
    }    

    public static void variante02(String[] args) 
    {

        float operando0, operando1;

        operando0 = 3;
        operando1 = 2;

        System.out.println( "Operando: " + String.valueOf(operando0) );
        System.out.println( "Operando: " + String.valueOf(operando1) );
        System.out.println( "------------------" );
        System.out.println( "Suma    : " + String.valueOf(operando0 + operando1) );
        System.out.println( "Resta   : " + String.valueOf(operando0 - operando1) );
        System.out.println( "Producto: " + String.valueOf(operando0 * operando1) );
        System.out.println( "Division: " + String.valueOf(operando0 / operando1) );
        System.out.println( "Cociente: " + String.valueOf(operando0 / operando1) );
        System.out.println( "Resto   : " + String.valueOf(operando0 % operando1) );
        System.out.println();
        
    }    

    public static void variante03(String[] args) 
    {
        String texto;
        float[] operando;
        
        operando = new float[2];
        operando[0] = (float)3.0;
        operando[1] = (float)2.0;

        System.out.println( "Operando: " + String.valueOf(operando[0]) );
        System.out.println( "Operando: " + String.valueOf(operando[1]) );
        System.out.println( "------------------" );
        System.out.println( "Suma    : " + String.valueOf(operando[0] + operando[1]) );
        System.out.println( "Resta   : " + String.valueOf(operando[0] - operando[1]) );
        System.out.println( "Producto: " + String.valueOf(operando[0] * operando[1]) );
        System.out.println( "Division: " + String.valueOf(operando[0] / operando[1]) );
        System.out.println();
        
        operando = null;
    }
    
    public static void variante04(String[] args) 
    {
        String texto;
        float[] operando;
        
        operando = new float[2];
        operando[0] = Float.parseFloat(JOptionPane.showInputDialog("Introduce el primer número:"));
        operando[1] = Float.parseFloat(JOptionPane.showInputDialog("Introduce el segundo número:"));

        System.out.println( "Operando: " + String.valueOf(operando[0]) );
        System.out.println( "Operando: " + String.valueOf(operando[1]) );
        System.out.println( "------------------" );
        System.out.println( "Suma    : " + String.valueOf(operando[0] + operando[1]) );
        System.out.println( "Resta   : " + String.valueOf(operando[0] - operando[1]) );
        System.out.println( "Producto: " + String.valueOf(operando[0] * operando[1]) );
        System.out.println( "Division: " + String.valueOf(operando[0] / operando[1]) );
        System.out.println();
        
        operando = null;
    }
}
