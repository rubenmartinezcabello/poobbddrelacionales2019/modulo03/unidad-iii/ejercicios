package General;

import javax.swing.JOptionPane;
import java.lang.NumberFormatException;

/**
 *
 * @author Rubén Martínez Cabello 71421914-Z
 */

public class ejercicio13
{
    protected static void enunciado()
    {
        System.out.println("Ejercicio 13");
        System.out.println("Realiza una aplicación que nos calcule una ecuación de segundo grado. Debes pedir las variables a, b y c por teclado y comprobar antes que el discriminante (operación en la raíz cuadrada). Para la raíz cuadrada usa el método sqlrt de Math. Te recomiendo que uses mensajes de traza.");
        System.out.println("================================================================================");
    }

    public static void variante01(String[] args) 
    {
        Double A,B,C, raiz, sol_pos, sol_neg;
        A = B = C = 0.0; 
 
        
        System.out.println("Sea una ecuación polinomial de segundo grado (cuadrática), en la forma (A)*x² + (B)*x¹ + (C)*x⁰ = 0");
        System.out.println();
        System.out.println("Introduce los valores de los coeficientes...");
        
        try
        {
            A = Double.parseDouble( JOptionPane.showInputDialog("Valor del coeficiente A:") );
            System.out.println("El coeficiente A vale " + A);
            B = Double.parseDouble( JOptionPane.showInputDialog("Valor del coeficiente B:") );
            System.out.println("El coeficiente B vale " + B);
            C = Double.parseDouble( JOptionPane.showInputDialog("Valor del coeficiente C:") );
            System.out.println("El coeficiente C vale " + C);
        }
        catch(NumberFormatException nfe)
        {
            System.out.println( "Lo siento, no has introducido algundo de los coeficientes correctamente.");
            System.exit(0);
        };

        System.out.println("Sea una ecuación polinomial de segundo grado (cuadrática), en la forma (" + String.format("%+8.4f", A) + ")*x² + (" + String.format("%+8.4f", B) + ")*x¹ + (" + String.format("%+8.4f", C) + ")*x⁰ = 0");
        
        if (A != 0.0)
        {
            raiz = Math.sqrt(Math.pow(B, 2) - 4 * A * C);
            sol_pos = (-B + raiz) / (2*A);
            sol_neg = (-B - raiz) / (2*A);
            System.out.println( "Las soluciones (2) son " + String.format("%+8.4f", sol_pos) + " y "+ String.format("%+8.4f", sol_neg) );
            // 2 5 3 
        }
        else if (B!=0)
        {
            sol_pos = -C/B;
            System.out.println( "La solucion (1) es " + String.format("%+8.4f", sol_pos) );
        }
        else if (C==0)
        {
            System.out.println( "No hay solucion: 0 = 0 ");
        }
        else
        {
            System.out.println( "Esto no tiene sentido: " + String.format("%+8.4f", C) + " = 0 ");
        };

    }
}