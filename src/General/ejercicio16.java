package General;

import java.util.Arrays;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Rubén Martínez Cabello 71421914-Z
 */

public class ejercicio16
{
    protected static void enunciado()
    {
        System.out.println("Ejercicio 16");
        System.out.println("Crea una aplicación que nos pida un día de la semana y que nos diga si es un día laboral o no. Si el día no se reconoce, mostrar el mensaje “Día incorrecto”. Usa un switch para ello.");
        System.out.println("================================================================================");
    }    

    public static void variante01(String[] args) 
    {
        String dias []= {"domingo", "lunes", "martes", "miercoles", "jueves", "viernes", "sabado" };
        String temp;
        Integer indice;
        

        temp = JOptionPane.showInputDialog("Introduce un dia de la semana:") ;
        for (indice = 0; indice < dias.length; indice++)
            if (temp.equals(dias[indice]))
                break;
        
        if (indice >= dias.length)
        {
            System.out.println("No he entendido que día dices. ¿" + temp + "?");
        }
        else
        {
            System.out.println("¿Es laborable el " + temp + "?");
            if ( (indice == 0) || (indice == 6) )
                System.out.println("Pues NO.");
            else
                System.out.println("Pues SI.");
        }

    }    
    
    public static void variante02(String[] args) 
    {
        
        List validos = Arrays.asList("domingo", "lunes", "martes", "miercoles", "jueves", "viernes", "sabado");
        List laborables = Arrays.asList("lunes", "martes", "miercoles", "jueves", "viernes");
        String temp;
        
        temp = JOptionPane.showInputDialog("Introduce un dia de la semana:") ;
        
        if (validos.contains(temp)) 
        {
            
            System.out.println("¿Es laborable el " + temp + "?");
            if (laborables.contains(temp)) 
            {
                System.out.println("Pues SI.");
            } 
            else 
            {
                System.out.println("Pues NO.");
            };
        } 
        else 
        {
            System.out.println("No he entendido que día dices. ¿" + temp + "?");
        };
    }    
}