package General;

import javax.swing.JOptionPane;

/**
 *
 * @author Rubén Martínez Cabello 71421914-Z
 */

public class ejercicio14
{
    protected static void enunciado()
    {
        System.out.println("Ejercicio 14");
        System.out.println("Lee un número por teclado y comprueba que este número es mayor o igual que cero, si no lo es lo volverá a pedir (do while), después muestra ese número por consola.");
        System.out.println("================================================================================");
        
        
    }    

    public static void variante01(String[] args) 
    {
        Integer positivo = -1;
        String calificativos[]  = { "zoquete", "burro", "idiota", "animal de bellota", "cazurro", "bestiajo", "cabezahueca"};

        do
        {
            try
            {
                positivo = Integer.parseInt( JOptionPane.showInputDialog("Introduce un número >= 0:") );
            }
            catch(NumberFormatException nfe)
            {
                System.out.println( "Te he dicho UN NÚMERO, " + calificativos[ 
                        ((int) (Math.random() * calificativos.length) )
                        
                        ] + ".");
                continue;
            };
            System.out.println( "Te he dicho MAYOR O IGUAL QUE CERO.");
        }
        while (positivo<0);
        
        System.out.println("El Niño de San Ildefonso ha dicho: \"¡EL " + positivo + "!\"");
    }    
}