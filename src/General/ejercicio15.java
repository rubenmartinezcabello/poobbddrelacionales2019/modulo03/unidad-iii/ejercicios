package General;

import javax.swing.JOptionPane;

/**
 *
 * @author Rubén Martínez Cabello 71421914-Z
 */

public class ejercicio15
{
    protected static void enunciado()
    {
        System.out.println("Ejercicio 15" + ejercicio08.class.getName() );
        System.out.println("Escribe una aplicación con un String que contenga una contraseña cualquiera. Después se te pedirá que introduzcas la contraseña, con 3 intentos. Cuando aciertes ya no pedirá más la contraseña y mostrará un mensaje diciendo “Enhorabuena”. Piensa bien en la condición de salida (3 intentos y si acierta sale, aunque le queden intentos)." );
        System.out.println("================================================================================");
        
        
    }    

    public static void variante01(String[] args) 
    {
        String password = "swordfish";
        String temp;
        Integer intentos = 3;
        Boolean login = false;
        
        while (intentos>0)
        {
            
            temp = JOptionPane.showInputDialog("Introduce la contraseña (te quedan " + intentos + " intentos):") ;
            System.out.println("Probamos " + temp);
            if (password.equals(temp))
            {
                login = true;
                break;
            }
            intentos--;
        };
        
        if (login)
            System.out.println("Felicidades, has logrado entrar.");
        else
            System.out.println("Habla con tu administrador, o tu confesor.");
    }    
}