package General;

import javax.swing.JOptionPane;

/**
 *
 * @author Rubén Martínez Cabello 71421914-Z
 */

public class ejercicio05 
{
    public static Integer número = 5;
    public static String  enunciado = "Haz una aplicación que calcule el área de un círculo(pi*R2). El radio se pedirá por teclado (recuerda pasar de String a double con Double.parseDouble). Usa la constante PI y el método pow de Math.";


    public static void printEnunciado()
    {
        System.out.format ("Ejercicio %02d%n", ejercicio01.número);
        System.out.println(ejercicio01.enunciado);
        System.out.println("================================================================================");
    }
    
    public static void variante01(String[] args) 
    {
        Double radio = 0.0;
        radio = Double.parseDouble(JOptionPane.showInputDialog("Introduce la medida del radio:"));
        System.out.println( " Radio     : " + (  radio) + "m");
        System.out.println( " Diametro  : " + (2*radio) + "m");
        System.out.println( " - CIRCUNFERENCIA -" );
        System.out.println( " Perimetro : " + ((2.0) * Math.PI * radio) + "m");
        System.out.println( " Superficie: " + ((1.0) * Math.PI * Math.pow(radio, 2)) + "m²");
        System.out.println( " - ESFERA -" );
        System.out.println( " Superficie   : " + ((4.0) * Math.PI * Math.pow(radio,2)) + "m²");
        System.out.println( " Volumen   : "   + ((4.0/3.0) * Math.PI * Math.pow(radio,3)) + "m³");
    }
    
}
