package General;

import javax.swing.JOptionPane;

/**
 *
 * @author Rubén Martínez Cabello 71421914-Z
 */

public class ejercicio04 
{
    public static Integer número = 4;
    public static String  enunciado = "Modifica la aplicación anterior, para que nos pida el nombre que queremos introducir (recuerda usar JOptionPane).";


    public static void printEnunciado()
    {
        System.out.format ("Ejercicio %02d%n", ejercicio01.número);
        System.out.println(ejercicio01.enunciado);
        System.out.println("================================================================================");
    }  
    
    public static void variante01(String[] args) 
    {
        String nombre = JOptionPane.showInputDialog("Introduce tu nombre:");
        System.out.println( "Bienvenido, " + nombre);
    }
    
}
