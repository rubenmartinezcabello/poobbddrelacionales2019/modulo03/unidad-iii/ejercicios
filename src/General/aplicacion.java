package General;

import swingforms.sfrmExercicePicker;
import java.util.Locale;
       
//import javax.swing.JOptionPane;
//import java.text.NumberFormat;

/**
 *
 * @author Rubén Martínez Cabello 71421914-Z
 * 
**/

public class aplicacion 
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        
        sfrmExercicePicker f = new sfrmExercicePicker();
        f.args = args;
        f.setVisible(true); // Antiguamente f.show(), este método ha sido deprecado
    }
    
    
    
    public static void runExercice(Integer exerciceId, String[] args)
    {
        switch (exerciceId)
        {
            case 0: 
                ejercicio01.printEnunciado();
                ejercicio01.variante01(args); 
                break;
            case 1: 
                ejercicio01.printEnunciado();
                ejercicio01.variante02(args); 
                break;
            case 2: 
                ejercicio01.printEnunciado();
                ejercicio01.variante03(args); 
                break;
            case 3: 
                ejercicio01.printEnunciado();
                ejercicio01.variante04(args); 
                break;
            case 4: 
                ejercicio02.printEnunciado();
                ejercicio02.variante01(args); 
                break;
            case 5: 
                ejercicio03.printEnunciado();
                ejercicio03.variante01(args); 
                break;
            case 6: 
                ejercicio04.printEnunciado();
                ejercicio04.variante01(args); 
                break;
            case 7: 
                ejercicio05.printEnunciado();
                ejercicio05.variante01(args); 
                break;
            case 8: 
                ejercicio06.printEnunciado();
                ejercicio06.variante01(args); 
                break;
            
            /*case 8: ejercicio07.variante01(args, new Locale("es","ES") ); break;
            case 9: ejercicio08.variante01(args); break;
            case 10: ejercicio09.variante01(args); break;
            case 11: ejercicio10.variante01(args); break;
            case 12: ejercicio11.variante01(args); break;
            case 13: ejercicio12.variante01(args); break;
            case 14: ejercicio13.variante01(args); break;
            case 15: ejercicio14.variante01(args); break;
            case 16: ejercicio15.variante01(args); break;
            case 17: ejercicio16.variante01(args); break;
            case 18: ejercicio16.variante02(args); break;*/
            default:
                System.out.println("Ejercicio no reconocido. ComboboxID = " + exerciceId);
                break;
        }



/*        //---------------------------
        ejercicio01.enunciado();
        ejercicio01.variante01(args);
        ejercicio01.variante02(args);
        ejercicio01.variante04(args);
        //---------------------------
        ejercicio02.enunciado();
        ejercicio02.variante01(args);
        //---------------------------
        ejercicio03.enunciado();
        ejercicio03.variante01(args);
        //---------------------------
        ejercicio04.enunciado();
        ejercicio04.variante01(args);
        //---------------------------
        ejercicio05.enunciado();
        ejercicio05.variante01(args);
        //---------------------------
        ejercicio06.enunciado();
        ejercicio06.variante01(args);
        //---------------------------
        ejercicio07.enunciado();
        ejercicio07.variante01(args, currentLocale);
        //---------------------------
        ejercicio08.enunciado();
        ejercicio08.variante01(args);
        //---------------------------
        ejercicio09.enunciado();
        ejercicio09.variante01(args);
        //---------------------------
        ejercicio10.enunciado();
        ejercicio10.variante01(args);
        //---------------------------
        ejercicio11.enunciado();
        ejercicio11.variante01(args);
        ejercicio11.variante02(args);
        //---------------------------
        ejercicio12.enunciado();
        ejercicio12.variante01(args);
        //---------------------------
        ejercicio13.enunciado();
        ejercicio13.variante01(args);
        //---------------------------
        ejercicio14.enunciado();
        ejercicio14.variante01(args);
        //---------------------------
        ejercicio15.enunciado();
        ejercicio15.variante01(args);
        //---------------------------
        ejercicio16.enunciado();
        ejercicio16.variante01(args);
        ejercicio16.variante02(args);
*/
    }
  
}
