package General;

import javax.swing.JOptionPane;

/**
 *
 * @author Rubén Martínez Cabello 71421914-Z
 */

public class ejercicio06 
{
    public static Integer número = 6;
    public static String  enunciado = "Lee un número por teclado e indica si es divisible entre 2 (resto = 0). Si no lo es, también debemos indicarlo.";


    public static void printEnunciado()
    {
        System.out.format ("Ejercicio %02d%n", ejercicio01.número);
        System.out.println(ejercicio01.enunciado);
        System.out.println("================================================================================");
    }
    
    public static void variante01(String[] args) 
    {
        Integer numero = 0;
        numero = Integer.parseInt(JOptionPane.showInputDialog("Introduce un número entero:"));
        if ((numero % 2)!= 0)
            JOptionPane.showMessageDialog(null, String.valueOf(numero) + " es IMPAR.", "Cálculo de paridad ", JOptionPane.INFORMATION_MESSAGE);
        else 
            JOptionPane.showMessageDialog(null, String.valueOf(numero) + " es PAR.",   "Cálculo de paridad ", JOptionPane.INFORMATION_MESSAGE);
    }
}
