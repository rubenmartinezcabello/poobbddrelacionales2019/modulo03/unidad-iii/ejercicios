package General;

/**
 *
 * @author Rubén Martínez Cabello 71421914-Z
 */

public class ejercicio11 
{
    protected static void enunciado()
    {
        System.out.println("Ejercicio 11");
        System.out.println("Muestra los números del 1 al 100 (ambos incluidos) divisibles entre 2 y 3. Utiliza el bucle que desees.");
        System.out.println("================================================================================");
        
        
    }    

    public static void variante01(String[] args) 
    {
        Integer acumulador = 1;
        for ( acumulador = 1; acumulador<=100; acumulador++)
        {
            if ( ((acumulador%2) == 0) || ((acumulador%3) == 0) )
                System.out.println( String.valueOf(acumulador));
        };
    }
    
    public static void variante02(String[] args) 
    {
        Integer acumulador = 1;
        String divisivilidad;
        for ( acumulador = 1; acumulador<=100; acumulador++)
        {
            divisivilidad = "";
            if ((acumulador%2) == 0) divisivilidad = " es divisible por 2.";
            if ((acumulador%3) == 0) divisivilidad = " es divisible por 3.";
            if ((acumulador%6) == 0) divisivilidad = " es divisible por 2 y 3.";
            if (!divisivilidad.isEmpty())
                System.out.println( String.valueOf(acumulador) + divisivilidad);
        };
    }    
}