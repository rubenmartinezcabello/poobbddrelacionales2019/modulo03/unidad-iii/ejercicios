/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swingforms;

import General.aplicacion;
import General.ejercicio01;
import javax.swing.*; 
import java.awt.*; 
import java.awt.event.*; 

/**
 *
 * @author Rubén Martínez Cabello 71421914-Z
 *
**/

public class sfrmExercicePicker 
    extends JFrame 
    implements ActionListener 
{ 
    // Componentes
    private Container c; 
    
    private JLabel lMenuEjercicio; 
    private JComboBox<String> cbMenuEjercicio; 
    private JButton btnEjecutar; 
    private String ejercicios [] = {
        "ejercicio01.variante01",
        "ejercicio01.variante02",
        "ejercicio01.variante03",
        "ejercicio01.variante04",
        
        "ejercicio02.variante01",
        
        "ejercicio03.variante01",
        
        "ejercicio04.variante01",
        
        "ejercicio05.variante01",
        
        "ejercicio06.variante01",
        
        "ejercicio07.variante01",
        
        "ejercicio08.variante01",
        
        "ejercicio09.variante01",
        
        "ejercicio10.variante01",
        
        "ejercicio11.variante01",
        "ejercicio11.variante02",
        
        "ejercicio12.variante01",
        
        "ejercicio13.variante01",
        
        "ejercicio14.variante01",
        
        "ejercicio15.variante01",
        
        "ejercicio16.variante01",
        "ejercicio16.variante02",
            
    };
    
    public String[] args = {};

    
    // constructor, to initialize the components 
    // with default values. 
    public sfrmExercicePicker() 
    { 
        setTitle("Escoge que ejercicio quieres utilizar:"); 
        
        setBounds(300, 90, 400, 200); 
        setDefaultCloseOperation(EXIT_ON_CLOSE); 
        setResizable(false); 

        c = getContentPane(); 
        c.setLayout(null); 
        
        
        lMenuEjercicio = new JLabel("Escoge el ejercicio y variante a ejecutar:"); 
        lMenuEjercicio.setFont(new Font("Arial", Font.PLAIN, 15)); 
        lMenuEjercicio.setSize(300, 30); 
        lMenuEjercicio.setLocation(50, 10); 
        c.add(lMenuEjercicio); 
        
        cbMenuEjercicio = new JComboBox<>(ejercicios); 
        cbMenuEjercicio.setFont(new Font("Arial", Font.PLAIN, 20)); 
        cbMenuEjercicio.setSize(300, 50); 
        cbMenuEjercicio.setLocation(50, 40); 
        c.add(cbMenuEjercicio); 
    
       
        btnEjecutar = new JButton("Submit"); 
        btnEjecutar.setFont(new Font("Arial", Font.PLAIN, 20)); 
        btnEjecutar.setSize(300, 40); 
        btnEjecutar.setLocation(50, 100); 
        btnEjecutar.addActionListener(this); 
        c.add(btnEjecutar);         

        //this.setVisible(true);
    }
    
    
    // method actionPerformed() 
    // to get the action performed 
    // by the user and act accordingly 
    public void actionPerformed(ActionEvent e) 
    {
        String s;
        //String[] action;
        
        if (e.getSource() ==  this.btnEjecutar)
        {
            
            aplicacion.runExercice( this.cbMenuEjercicio.getSelectedIndex(), this.args );
            
            
            //s = this.cbMenuEjercicio.getItemAt( this.cbMenuEjercicio.getSelectedIndex() ).toString();
            //action = s.split("\\.");
            /*
            if (s.equals("ejercicio01.variante01"))
            {
                ejercicio01.variante01(args);
            }
            
            
            
            // Invocar a un método estático pasando por valor sus parametros
/*            try
            {
                //ejercicio01.enunciado();
                Class myClass = Class.forName("General.ejercicio01"); //action[0]
                System.out.println("Loaded class: " + myClass);
                
                Method myMethod =  myClass.getDeclaredMethod("enunciado");
                //System.out.println("Got method: " + myMethod);
                
                //Object myObject = myMethod.invoke(null, null);
                
                /*Method myMethod = myClass.getDeclaredMethod(action[1], null);
                 System.out.println("Got method: " + myMethod);
                 
                Object myObject = myMethod.invoke(null, null);
                System.out.println("Output: " + o);
            }
            catch (Exception ex)
            {
                System.out.println("EXCEPCION- HA HABIDO UN FALLO");
                ex.printStackTrace();
            };*/
        
            
            System.exit(0);

        };
    } 
    
}
